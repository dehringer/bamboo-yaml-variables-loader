[@ww.textfield labelKey='yaml.loader.variableType.file' descriptionKey='yaml.loader.variableType.file.description' name='yaml_file' required='true' cssClass="long-field" /]

[@ww.radio labelKey='yaml.loader.variableType' name='variableType'
           listKey='key' listValue='value'
           list=variableTypeOptions ]
[/@ww.radio]

<script type="text/javascript">
(function ($) {
	var helpIcon = $("#variableContextHelp");
	var helpDialog = AJS.InlineDialog(helpIcon, "contextHelpDialog",
		    function(content, trigger, showPopup) {
		        content.css({"padding":"20px"}).html('<b>Job</b> scoped variables have local scope and they cease to exist when the job finishes.<br/><br/><b>Result</b> scoped variables persist beyond the execution of a Job and are passed into subsequent stages or related deployment releases.</dd></dl>');
		        showPopup();
		        return false;
		    }
		);
}(jQuery));
</script>