package org.gaptap.bamboo.yaml;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskConfiguratorHelper;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.struts.TextProvider;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;

public class YamlLoaderTaskConfigurator extends AbstractTaskConfigurator {

    public static final String VARIABLE_TYPE_OPTIONS = "variableTypeOptions";
    public static final String VARIABLE_TYPE_JOB = "job";
    public static final String VARIABLE_TYPE_RESULT = "result";
    public static final String SELECTED_VARIABLE_TYPE = "variableType";

    public static final String YAML_FILE = "yaml_file";

    private static final List<String> FIELDS_TO_COPY = ImmutableList.of(SELECTED_VARIABLE_TYPE, YAML_FILE);

    private final TextProvider textProvider;

    public YamlLoaderTaskConfigurator(TextProvider textProvider, TaskConfiguratorHelper taskConfiguratorHelper) {
        this.textProvider = textProvider;
        this.taskConfiguratorHelper = taskConfiguratorHelper;
    }

    @Override
    @NotNull
    public Map<String, String> generateTaskConfigMap(@NotNull ActionParametersMap params,
                                                     @Nullable TaskDefinition previousTaskDefinition) {
        Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        taskConfiguratorHelper.populateTaskConfigMapWithActionParameters(config, params, FIELDS_TO_COPY);
        return config;
    }

    @Override
    public void populateContextForCreate(@NotNull Map<String, Object> context) {
        super.populateContextForCreate(context);
        populateContextForAll(context);
        context.put(SELECTED_VARIABLE_TYPE, VARIABLE_TYPE_RESULT);
    }

    @Override
    public void populateContextForEdit(@NotNull Map<String, Object> context, @NotNull TaskDefinition taskDefinition) {
        super.populateContextForEdit(context, taskDefinition);
        populateContextForAll(context);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    private void populateContextForAll(@NotNull final Map<String, Object> context) {
        Map<String, String> variableTypeOptions = Maps.newHashMap();
        variableTypeOptions.put(VARIABLE_TYPE_JOB,
                textProvider.getText("yaml.loader.variableType.job"));
        variableTypeOptions.put(VARIABLE_TYPE_RESULT,
                textProvider.getText("yaml.loader.variableType.result"));
        context.put(VARIABLE_TYPE_OPTIONS, variableTypeOptions);
    }

    @Override
    public void validate(@NotNull ActionParametersMap params, @NotNull ErrorCollection errorCollection) {
        super.validate(params, errorCollection);
        validateRequiredNotBlank(YAML_FILE, params, errorCollection);
        validateRequiredNotBlank(SELECTED_VARIABLE_TYPE, params, errorCollection);
    }

    private void validateRequiredNotBlank(String field, ActionParametersMap params, ErrorCollection errorCollection) {
        if (StringUtils.isBlank(params.getString(field))) {
            errorCollection.addError(field, textProvider.getText("yaml.loader.global.required.field"));
        }
    }
}
