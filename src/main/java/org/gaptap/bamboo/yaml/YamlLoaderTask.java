package org.gaptap.bamboo.yaml;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.CommonTaskType;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskResultBuilder;
import com.atlassian.bamboo.variable.VariableContext;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Map;

import static org.gaptap.bamboo.yaml.YamlLoaderTaskConfigurator.SELECTED_VARIABLE_TYPE;
import static org.gaptap.bamboo.yaml.YamlLoaderTaskConfigurator.VARIABLE_TYPE_JOB;
import static org.gaptap.bamboo.yaml.YamlLoaderTaskConfigurator.VARIABLE_TYPE_RESULT;
import static org.gaptap.bamboo.yaml.YamlLoaderTaskConfigurator.YAML_FILE;

public class YamlLoaderTask implements CommonTaskType {

    private final PropertiesConverter propertiesConverter = new PropertiesConverter();

    public TaskResult execute(CommonTaskContext taskContext) throws TaskException {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        try {
            File yamlFile = getYamlFile(taskContext);
            Object yaml = getYaml(yamlFile);
            Map<String, String> variables = propertiesConverter.flatten(yaml);
            saveVariables(variables, taskContext);
        } catch (Exception e) {
            buildLogger.addErrorLogEntry("Unable to load properties from YAML file: " + e.getMessage());
            return TaskResultBuilder.newBuilder(taskContext).failedWithError().build();
        }
        return TaskResultBuilder.newBuilder(taskContext).success().build();
    }

    private File getYamlFile(CommonTaskContext taskContext) {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        String yamlFile = taskContext.getConfigurationMap().get(YAML_FILE);
        File file = new File(taskContext.getRootDirectory(), yamlFile);
        buildLogger.addBuildLogEntry("Loading properties from " + file.getAbsolutePath());
        return file;
    }

    private Object getYaml(File file) throws FileNotFoundException {
        Yaml yaml = new Yaml();
        return yaml.load(new FileInputStream(file));
    }

    private void saveVariables(Map<String, String> variables, CommonTaskContext taskContext) {
        BuildLogger buildLogger = taskContext.getBuildLogger();
        String variableType = taskContext.getConfigurationMap().get(SELECTED_VARIABLE_TYPE);
        buildLogger.addBuildLogEntry("Setting variables as " + variableType + " variables.");
        for(Map.Entry<String, String> entry: variables.entrySet()){
            buildLogger.addBuildLogEntry("Setting " + entry.getKey() + ":" + redact(entry.getValue()));
            VariableContext variableContext = taskContext.getCommonContext().getVariableContext();
            if(VARIABLE_TYPE_RESULT.equals(variableType)){
                variableContext.addResultVariable(entry.getKey(), entry.getValue());
            } else if(VARIABLE_TYPE_JOB.equals(variableType)) {
                variableContext.addLocalVariable(entry.getKey(), entry.getValue());
            } else {
                throw new IllegalArgumentException("Unknown variable type: " + variableType);
            }
        }
    }

    private String redact(String value) {
        StringBuilder redacted = new StringBuilder();
        for(char c: value.toCharArray()){
            redacted.append("*");
        }
        return redacted.toString();
    }
}
