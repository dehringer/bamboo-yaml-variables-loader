package org.gaptap.bamboo.yaml;

import com.google.common.collect.Maps;
import org.apache.commons.lang.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PropertiesConverter {

    public Map<String, String> flatten(Object input) {
        return extract(input, "");
    }

    private Map<String, String> extract(final Object input, final String baseKey){
        if(input instanceof Map){
            Map<String, String> result = Maps.newHashMap();
            for(Map.Entry<String, Object> e: ((Map<String, Object>) input).entrySet()){
                result.putAll(extract(e.getValue(), buildKey(baseKey, e.getKey())));
            }
            return result;
        } else if (input instanceof List){
            Map<String, String> result = Maps.newHashMap();
            List entries = (List)input;
            for(int i = 0; i < entries.size(); i++){
                result.putAll(extract(entries.get(i), baseKey + "[" + i + "]"));
            }
            return result;
        } else if (isLeaf(input)){
            return new HashMap<String, String>(){{
                put(baseKey, String.valueOf(input));
            }};
        }
        return Maps.newHashMap();
    }

    private String buildKey(String baseKey, String key){
        if(StringUtils.isBlank(baseKey)){
            return key;
        }
        return baseKey + "." + key;
    }

    private boolean isLeaf(Object value) {
        return value instanceof String ||
                value instanceof Number;
    }
}
