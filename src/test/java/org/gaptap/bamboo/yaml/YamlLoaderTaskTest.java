package org.gaptap.bamboo.yaml;

import com.atlassian.bamboo.build.logger.BuildLogger;
import com.atlassian.bamboo.build.logger.Log4jBuildLogger;
import com.atlassian.bamboo.configuration.ConfigurationMap;
import com.atlassian.bamboo.configuration.ConfigurationMapImpl;
import com.atlassian.bamboo.task.CommonTaskContext;
import com.atlassian.bamboo.task.TaskException;
import com.atlassian.bamboo.task.TaskResult;
import com.atlassian.bamboo.task.TaskState;
import com.atlassian.bamboo.v2.build.CommonContext;
import com.atlassian.bamboo.variable.VariableContext;
import com.atlassian.bamboo.variable.VariableDefinitionContext;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.util.HashMap;

import static org.gaptap.bamboo.yaml.YamlLoaderTaskConfigurator.SELECTED_VARIABLE_TYPE;
import static org.gaptap.bamboo.yaml.YamlLoaderTaskConfigurator.VARIABLE_TYPE_JOB;
import static org.gaptap.bamboo.yaml.YamlLoaderTaskConfigurator.VARIABLE_TYPE_RESULT;
import static org.gaptap.bamboo.yaml.YamlLoaderTaskConfigurator.YAML_FILE;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.RETURNS_SMART_NULLS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class YamlLoaderTaskTest {

    private CommonTaskContext taskContext;
    private BuildLogger buildLogger = new Log4jBuildLogger();

    private ConfigurationMap configurationMap;
    private VariableContext variableContext;

    private YamlLoaderTask task;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);

        taskContext = mock(CommonTaskContext.class, RETURNS_SMART_NULLS);
        createCommonContext();
        createDefaultConfigMap();

        when(taskContext.getRootDirectory()).thenReturn(new File("."));
        when(taskContext.getBuildLogger()).thenReturn(buildLogger);

        task = new YamlLoaderTask();
    }

    private void createCommonContext() {
        CommonContext commonContext = mock(CommonContext.class);
        when(taskContext.getCommonContext()).thenReturn(commonContext);

        variableContext = mock(VariableContext.class);
        when(commonContext.getVariableContext()).thenReturn(variableContext);

        when(variableContext.getDefinitions()).thenReturn(new HashMap<String, VariableDefinitionContext>());
    }

    private void createDefaultConfigMap() {
        configurationMap = new ConfigurationMapImpl();
        when(taskContext.getConfigurationMap()).thenReturn(configurationMap);
    }

    @Test
    public void resultVariablesAreSet() throws TaskException {
        // Given
        configurationMap.put(YAML_FILE, "src/test/resources/fixtures/map.yml");
        configurationMap.put(SELECTED_VARIABLE_TYPE, VARIABLE_TYPE_RESULT);

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        verify(variableContext).addResultVariable("applications[0].name", "my-app");
        verify(variableContext).addResultVariable("applications[0].routes[0].route", "example.com");
        verify(variableContext).addResultVariable("applications[0].routes[1].route", "www.example.com/foo");
        verify(variableContext).addResultVariable("applications[0].routes[2].route", "tcp-example.com:1234");
        // and so on
    }

    @Test
    public void jobVariablesAreSet() throws TaskException {
        // Given
        configurationMap.put(YAML_FILE, "src/test/resources/fixtures/map.yml");
        configurationMap.put(SELECTED_VARIABLE_TYPE, VARIABLE_TYPE_JOB);

        // When
        TaskResult result = task.execute(taskContext);

        // Then
        assertThat(result.getTaskState(), is(TaskState.SUCCESS));
        verify(variableContext).addLocalVariable("applications[0].name", "my-app");
        verify(variableContext).addLocalVariable("applications[0].routes[0].route", "example.com");
        verify(variableContext).addLocalVariable("applications[0].routes[1].route", "www.example.com/foo");
        verify(variableContext).addLocalVariable("applications[0].routes[2].route", "tcp-example.com:1234");
        // and so on
    }
}
