package org.gaptap.bamboo.yaml;

import org.junit.Test;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.util.Map;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class PropertiesConverterTest {

    private final PropertiesConverter propertiesConverter = new PropertiesConverter();

    private Object loadYaml(String file) throws FileNotFoundException {
        InputStream inputStream = new FileInputStream(new File(file));
        Yaml yaml = new Yaml();
        return yaml.load(inputStream);
    }

    @Test
    public void mapAsRoot() throws FileNotFoundException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Object input = loadYaml("src/test/resources/fixtures/map.yml");

        Map<String, String> flat = propertiesConverter.flatten(input);

        assertThat(flat.get("applications[0].name"), is("my-app"));
        assertThat(flat.get("applications[0].routes[0].route"), is("example.com"));
        assertThat(flat.get("applications[0].routes[1].route"), is("www.example.com/foo"));
        assertThat(flat.get("applications[0].routes[2].route"), is("tcp-example.com:1234"));
        assertThat(flat.get("applications[0].env.greeting"), is("hello"));
        assertThat(flat.get("applications[0].env.BUNDLE_WITHOUT"), is("test:development"));
        assertThat(flat.get("stuff"), is("other"));
    }

    @Test
    public void arrayAsRoot() throws FileNotFoundException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Object input = loadYaml("src/test/resources/fixtures/array.yml");

        Map<String, String> flat = propertiesConverter.flatten(input);

        assertThat(flat.get("[0]"), is("Casablanca"));
        assertThat(flat.get("[1]"), is("North by Northwest"));
        assertThat(flat.get("[2]"), is("The Man Who Wasn't There"));
        assertThat(flat.get("[3].weird.movie"), is("name"));
    }

    @Test
    public void json() throws FileNotFoundException, IllegalAccessException, NoSuchMethodException, InvocationTargetException {
        Object input = loadYaml("src/test/resources/fixtures/example.json");

        Map<String, String> flat = propertiesConverter.flatten(input);

        assertThat(flat.get("services[0]"), is("service1"));
        assertThat(flat.get("services[1]"), is("service2"));
        assertThat(flat.get("applications.test-app.one"), is("value"));
        assertThat(flat.get("applications.test-app.two"), is("value2"));
    }
}
