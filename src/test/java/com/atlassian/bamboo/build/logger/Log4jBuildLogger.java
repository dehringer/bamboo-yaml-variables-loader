package com.atlassian.bamboo.build.logger;

import com.atlassian.bamboo.Key;
import com.atlassian.bamboo.build.LogEntry;
import com.atlassian.bamboo.utils.expirables.ExpiryTickerImpl;

public class Log4jBuildLogger extends AbstractBuildLogger {

    private static final org.apache.log4j.Logger LOG = org.apache.log4j.Logger.getLogger("BambooBuildLog");

    public Log4jBuildLogger() {
        super(LoggerIds.get(new StubbedKey()), new ExpiryTickerImpl());
    }

    @Override
    public void onAddLogEntry(LogEntry logEntry) {
        LOG.info(logEntry.getLog());
    }

    @Override
    public void stopStreamingBuildLogs() {

    }

    private static class StubbedKey implements Key {

        @Override
        public String getKey() {
            return "stub";
        }
    }
}
